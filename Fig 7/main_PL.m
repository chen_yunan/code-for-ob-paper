%% Written by Yunan Chen on 03.06.2018, for phase syn. calculation
% Modified March 2020,with new structure&value
function [main]=main_PL(sig1,sig2,m,n) 
%Input: sig1,sig2 = different signals of interest for phase angle  
%Optional: m,n = cycles respectively for two signals with chananging ranges
%(for PSC both m&n could be autofilled as default 1)
if nargin<3
m=1;n=1;
   elseif nargin<4
   n=m;
end
bin=3; % size
phase_sig1=angle(hilbert(sig1));phase_1= buffer(phase_sig1,length(phase_sig1)/bin,length(phase_sig1)/bin*.5,'nodelay')';
phase_sig2=angle(hilbert(sig2));phase_2= buffer(phase_sig2,length(phase_sig2)/bin,length(phase_sig1)/bin*.5,'nodelay')';
[S,N] = size(phase_1);  
for j=1:S
  figure, %sgtitle('phase-phase') 
  for k=1:length(n)
     nk=n(k);
     parfor l=1:length(m)
     ml=m(l);
     RP=nk*phase_1(j,:)- ml*phase_2(j,:); 
% compute length
e = exp(1i*(RP));
pl(l,k,j) = abs(sum(e,2)) / N;
     end
mn(:,k,j)=m./nk;subplot(1,length(n),k);plot(mn(:,k,j),pl(:,k,j));title(['n=',num2str(nk)])
  end
end
main.mn=mean(mn,3);main.pl=mean(pl,3);
end
