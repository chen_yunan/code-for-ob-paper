%% Written by Yunan Chen on 16.04.2018, for cross-spectral coherence
function [cxy,freq]=main_CSC(sig1,sig2,len,fs) 
%Input: sig1,sig2 = different signals of interest (in column); len = window size of
%segment; fs = sampling rate;
param.Fs=fs;param.pad=0;
[cxy, ~, ~, ~, ~, freq, ~]=coherencysegc(sig1,sig2,len, param);
end
