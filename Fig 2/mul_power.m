%% Written by Yunan Chen on 02.01.2018, for power spectral density
function [pow,freq] = mul_power(sig,Cb,Fb,param)
%%load in and multitaper estimation
sig_l = BPfilter(sig,param.Fs,Cb,3,'but','twopass','no');
[pxx,f] = mtspectrumc(sig_l, param);
[freq] = rsm(f,param.bin);
[pow.all] = rsm(pxx,param.bin);
[pow.main,pow.field] = cal_pow(pow.all,Fb,freq);
figure(1)
plot(f,pxx)
end

function [datanew]=rsm(data,bin)
if size(data,2)==1,data=data';end
remainder=rem(length(data),bin);
if remainder~=0;
    data(end+1-remainder:end)=[];
end 

datanew=reshape(data,length(data)/bin,bin);
datanew=mean(datanew,1);
datanew=datanew'

end

function [mp,ap] = cal_pow(p,bp,f)
mp=mean(p(f>=bp(1)&f<=bp(2)));
ap=trapz(f(f>=bp(1)&f<=bp(2)),p(f>=bp(1)&f<=bp(2)))
end